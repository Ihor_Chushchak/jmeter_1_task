/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 6;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 99.86666666666666, "KoPercent": 0.13333333333333333};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.7890625, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.625, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/welcome.pl?signOff=1"], "isController": false}, {"data": [0.95, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/nav.pl?page=menu&in=itinerary"], "isController": false}, {"data": [0.885, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/nav.pl?page=menu&in=flights"], "isController": false}, {"data": [0.83, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/reservations.pl?page=welcome"], "isController": false}, {"data": [0.0, 500, 1500, "Test"], "isController": true}, {"data": [0.9, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/login.pl?intro=true"], "isController": false}, {"data": [0.69, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/welcome.pl?page=itinerary"], "isController": false}, {"data": [0.655, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/welcome.pl?page=search"], "isController": false}, {"data": [0.825, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/reservations.pl"], "isController": false}, {"data": [1.0, 500, 1500, "http:\/\/localhost:1080\/WebTours\/home.html"], "isController": false}, {"data": [0.955, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/login.pl"], "isController": false}, {"data": [0.87, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/nav.pl?page=menu&in=home"], "isController": false}, {"data": [0.93, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/nav.pl?in=home"], "isController": false}, {"data": [0.86, 500, 1500, "http:\/\/localhost:1080\/cgi-bin\/itinerary.pl"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 1500, 2, 0.13333333333333333, 432.9440000000003, 2, 1014, 636.0, 704.9000000000001, 837.0, 44.103378318779214, 140.79176589926786, 21.20637440827967], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Transactions\/s", "Received", "Sent"], "items": [{"data": ["http:\/\/localhost:1080\/cgi-bin\/welcome.pl?signOff=1", 100, 0, 0.0, 583.1200000000001, 233, 1014, 753.9, 853.8, 1012.9199999999994, 3.5229874933943983, 3.526943973489519, 1.4580489254888145], "isController": false}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/nav.pl?page=menu&in=itinerary", 100, 0, 0.0, 401.73000000000013, 218, 664, 505.9000000000001, 530.55, 663.3599999999997, 3.497604141163303, 5.974959886852506, 1.4099716694064566], "isController": false}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/nav.pl?page=menu&in=flights", 100, 0, 0.0, 418.7999999999999, 206, 819, 575.6000000000001, 671.3999999999996, 818.0099999999995, 3.511482547931737, 5.998908422729826, 1.4251681122269821], "isController": false}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/reservations.pl?page=welcome", 100, 0, 0.0, 485.7099999999998, 263, 858, 686.5, 768.8499999999999, 857.5799999999998, 3.495281370150297, 15.429243763107305, 1.4220060730513806], "isController": false}, {"data": ["Test", 100, 2, 2.0, 6494.16, 5338, 7946, 7123.1, 7360.949999999999, 7942.409999999998, 2.931347833733951, 140.3666749721522, 21.14234625080612], "isController": true}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/login.pl?intro=true", 100, 1, 1.0, 428.86, 173, 764, 590.0000000000001, 617.55, 763.92, 3.479107956719897, 3.9094097367185054, 1.4500813241484884], "isController": false}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/welcome.pl?page=itinerary", 100, 0, 0.0, 541.3299999999998, 292, 837, 721.6000000000001, 788.5999999999997, 836.8299999999999, 3.4769305656966027, 2.77845460215222, 1.4559646743854526], "isController": false}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/welcome.pl?page=search", 100, 0, 0.0, 572.9700000000001, 334, 965, 776.2, 836.95, 964.6599999999999, 3.476205374213509, 2.872554163624987, 1.459055732262662], "isController": false}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/reservations.pl", 300, 1, 0.3333333333333333, 475.6633333333334, 211, 847, 627.6000000000001, 690.95, 808.95, 10.068465565847765, 27.540793017519132, 7.357321452543966], "isController": false}, {"data": ["http:\/\/localhost:1080\/WebTours\/home.html", 100, 0, 0.0, 43.7, 2, 103, 49.0, 50.0, 102.93999999999997, 3.632928867252779, 5.886338143936642, 1.4297561850613965], "isController": false}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/login.pl", 100, 0, 0.0, 334.5800000000002, 95, 688, 495.6, 553.55, 687.8799999999999, 3.443526170798898, 3.357438016528926, 1.8461873708677687], "isController": false}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/nav.pl?page=menu&in=home", 100, 0, 0.0, 436.5900000000001, 145, 935, 564.9, 642.4999999999997, 934.0499999999995, 3.497726477789437, 5.974861457240294, 1.4749202081147255], "isController": false}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/nav.pl?in=home", 100, 0, 0.0, 380.78, 111, 759, 543.6000000000001, 597.75, 758.97, 3.5751313860784384, 6.1266370191448285, 1.4209750723964105], "isController": false}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/itinerary.pl", 100, 0, 0.0, 439.0, 216, 807, 579.9, 639.9999999999998, 805.9299999999995, 3.5023816195012607, 77.33337282633441, 1.3537525830064443], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["500\/Internal Server Error", 2, 100.0, 0.13333333333333333], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 1500, 2, "500\/Internal Server Error", 2, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/login.pl?intro=true", 100, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["http:\/\/localhost:1080\/cgi-bin\/reservations.pl", 300, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
